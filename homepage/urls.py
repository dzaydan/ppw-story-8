from django.urls import path
# from .views import *
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('data/', views.data, name='books_data'),

]